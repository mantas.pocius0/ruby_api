Rails.application.routes.draw do
  resources :invoices, only: :create
  resources :contracts, only: :create
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
