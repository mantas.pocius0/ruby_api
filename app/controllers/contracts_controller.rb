class ContractsController < ApplicationController

  # POST /contracts
  def create
    @contract = Contract.new(contract_params)

    if @contract.save
      render json: @contract, status: :created
    else
      render json: @contract.errors, status: :unprocessable_entity
    end
  end

  private

    # Only allow a trusted parameter "white list" through.
    def contract_params
      params.require(:contract).permit(:number, :active, :start_date, :end_date, :fixed_fee, :days_included, :additional_fee)
    end
end
