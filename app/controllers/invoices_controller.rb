class InvoicesController < ApplicationController

  # POST /invoices
  def create
    @invoice = Invoice.new(invoice_params)
    if @invoice.save
      render json: @invoice, status: :created
    else
      render json: @invoice.errors, status: :unprocessable_entity
    end
  end

  private

    # Only allow a trusted parameter "white list" through.
    def invoice_params
      params.require(:invoice).permit(:contract_number, :issue_date, :due_date, :paid_date, :purchase_date, :invoice_amount)
    end
end
