class Invoice < ApplicationRecord
  belongs_to :contract

  validates_presence_of :issue_date, :due_date, :purchase_date, :invoice_amount
  attr_accessor :contract_number

  after_initialize :find_contract
  before_save :calculate_fee


  private
  
  def find_contract
    self.contract = Contract.active.number(self.contract_number).applies_for(self.issue_date).first
  end
  
  def calculate_fee
    self.fee = calculate_fixed_fee + calculate_additional_fee
  end

  def calculate_fixed_fee
    (self.contract.fixed_fee / 100) * self.invoice_amount
  end
  
  def calculate_additional_fee
    fee_days = (self.paid_date || Date.today) - (self.purchase_date + self.contract.days_included) + 1

    fee_days * (self.contract.additional_fee / 100) * self.invoice_amount
  end
  
end
