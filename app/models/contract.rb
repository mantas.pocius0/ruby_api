class Contract < ApplicationRecord
  validates_presence_of :number, :start_date, :fixed_fee, :days_included, :additional_fee
  validates_inclusion_of :active, in: [true, false]
  validates_numericality_of :days_included, greater_than_or_equal_to: 0.0
  validate :end_date_not_before_start_date

  scope :active, -> () { where(active: true)}
  scope :number, -> (number) { where(number: number)}
  scope :applies_for, -> (issue_date) { where("? BETWEEN start_date AND end_date", issue_date)}
  scope :overlapping, -> (contract) {
    if contract.end_date.present?
      active
      .where(number: contract.number)
      .where("(start_date <= :end_date AND (end_date IS NULL OR end_date >= :start_date))", {start_date: contract.start_date, end_date: contract.end_date})
    else
      active
      .where(number: contract.number)
      .where("end_date >= :start_date OR end_date IS NULL", {start_date: contract.start_date})
    end
  }

  before_create :update_overlapping_contracts

  def fully_overlapped
    self.update(active: false) 
  end

  def start_overlapped(new_contract)
    self.update(end_date: new_contract.start_date - 1)
  end

  def end_overlapped(new_contract)
    self.update(start_date: new_contract.end_date + 1)
  end

  def contract_splitted(new_contract)
    self.duplicate_with_new_start_date(new_contract.end_date + 1)
    self.update(end_date: new_contract.start_date - 1)
  end

  def duplicate_with_new_start_date(start_date)
    new_contract = self.dup
    new_contract.start_date = start_date
    new_contract.save
  end
  
  
  

  private

  def end_date_not_before_start_date
    errors.add(:start_date, "after the end_date") if self.end_date.present? && self.end_date < self.start_date
  end

  def update_overlapping_contracts
    overlapping_contracts = Contract.overlapping(self)
    overlapping_contracts.each do |overlapping_contract|
      old_end_date = overlapping_contract.end_date
      new_end_date = self.end_date
      old_start_more_or_equal_than_new = overlapping_contract.start_date >= self.start_date
      old_end_less_or_equal_than_new = 
        (old_end_date.blank? && new_end_date.blank?) || 
        (old_end_date.present? && new_end_date.blank?) || 
        (old_end_date.present? && old_end_date <= new_end_date)

      overlapping_contract.fully_overlapped if old_start_more_or_equal_than_new && old_end_less_or_equal_than_new
      overlapping_contract.start_overlapped(self) if !old_start_more_or_equal_than_new && old_end_less_or_equal_than_new
      overlapping_contract.end_overlapped(self) if old_start_more_or_equal_than_new && !old_end_less_or_equal_than_new
      overlapping_contract.contract_splitted(self) if !old_start_more_or_equal_than_new && !old_end_less_or_equal_than_new

    end
  end
end
