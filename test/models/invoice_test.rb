require 'test_helper'

class InvoiceTest < ActiveSupport::TestCase
  test "should validate required fields" do
    invoice = Invoice.new

    assert_not invoice.valid?
    assert_equal [:contract, :issue_date, :due_date, :purchase_date, :invoice_amount], invoice.errors.keys
  end

  test "should calculate fee when paid date not specified" do
    contract = contracts(:one)
    invoice = Invoice.create(
      contract_number: contract.number,
      issue_date: contract.start_date,
      purchase_date: contract.start_date + 4,
      due_date: contract.start_date + 14,
      invoice_amount: 1000
    )

    fixed_fee = (contract.fixed_fee / 100) * invoice.invoice_amount
    fee_days = Date.today - (invoice.purchase_date + contract.days_included) + 1
    additional_fee = fee_days * (contract.additional_fee / 100) * invoice.invoice_amount


    assert_equal invoice.fee, fixed_fee + additional_fee
  end
end
