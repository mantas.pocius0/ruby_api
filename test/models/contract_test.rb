require 'test_helper'

class ContractTest < ActiveSupport::TestCase
  test "should validate required fields" do
    contract = Contract.new

    assert_not contract.valid?
    assert_equal [:number, :start_date, :fixed_fee, :days_included, :additional_fee, :active], contract.errors.keys
  end

  test "should validate numericality of days included" do
    contract = Contract.new(days_included: -1)

    assert_not contract.valid?
    assert_equal contract.errors.messages[:days_included], ["must be greater than or equal to 0.0"]
  end

  test "should validate dates when end_date is before start_date" do
    date = Date.today
    contract = Contract.new(start_date: date, end_date: date - 1)

    assert_not contract.valid?
    assert_equal contract.errors.messages[:start_date], ["after the end_date"]
  end

  test "should validate dates when end_date is nil" do
    date = Date.today
    contract = Contract.new(start_date: date)
    
    assert_not contract.valid?
    assert contract.errors.messages[:start_date].blank?
  end

  test "should deactivate fully overlapped contract when both end_dates are nil" do
    contract2 = contracts(:two)
    contract = Contract.create(number: "A10", active: true, start_date: "2020-01-01", end_date: nil, fixed_fee: 1.9, days_included: 14, additional_fee: 0.1)
    
    assert_equal contract2.reload.active, false
  end
  
  test "should deactivate fully overlapped contract when new contract end_date is nil" do
    contract1 = contracts(:one)
    contract = Contract.create(number: "A10", active: true, start_date: "2019-01-01", end_date: nil, fixed_fee: 1.9, days_included: 14, additional_fee: 0.1)
    
    assert_equal contract1.reload.active, false
  end

  test "should deactivate fully overlapped contract" do
    contract1 = contracts(:one)
    contract = Contract.create(number: "A10", active: true, start_date: "2019-01-01", end_date: "2019-09-30", fixed_fee: 1.9, days_included: 14, additional_fee: 0.1)
    
    assert_equal contract1.reload.active, false
  end
  
  test "should update overlapped(end) contract when old contract end_date is nil" do
    contract2 = contracts(:two)
    contract = Contract.create(number: "A10", active: true, start_date: "2020-01-01", end_date: "2020-03-01", fixed_fee: 1.9, days_included: 14, additional_fee: 0.1)
    
    contract2.reload
    assert_equal contract2.active, true
    assert_equal contract2.start_date, contract.end_date + 1
  end

  test "should update overlapped(end) contract" do
    contract1 = contracts(:one)
    contract = Contract.create(number: "A10", active: true, start_date: "2018-01-01", end_date: "2019-03-01", fixed_fee: 1.9, days_included: 14, additional_fee: 0.1)
    
    contract1.reload
    assert_equal contract1.active, true
    assert_equal contract1.start_date, contract.end_date + 1
  end

  test "should update overlapped(start) contract when both end_dates are nil" do
    contract2 = contracts(:two)
    contract = Contract.create(number: "A10", active: true, start_date: "2020-02-01", end_date: nil, fixed_fee: 1.9, days_included: 14, additional_fee: 0.1)

    contract2.reload
    assert_equal contract2.active, true
    assert_equal contract2.end_date, contract.start_date - 1
  end

  test "should update overlapped(start) contract when new contract end_date is nil" do
    contract1 = contracts(:one)
    contract = Contract.create(number: "A10", active: true, start_date: "2019-02-01", end_date: nil, fixed_fee: 1.9, days_included: 14, additional_fee: 0.1)

    contract1.reload
    assert_equal contract1.active, true
    assert_equal contract1.end_date, contract.start_date - 1
  end

  test "should update overlapped(start) contract" do
    contract1 = contracts(:one)
    contract = Contract.create(number: "A10", active: true, start_date: "2019-02-01", end_date: "2019-10-30", fixed_fee: 1.9, days_included: 14, additional_fee: 0.1)

    contract1.reload
    assert_equal contract1.active, true
    assert_equal contract1.end_date, contract.start_date - 1
  end

  test "should split old contract when old contract date is nil" do
    contract = nil
    contract2 = contracts(:two)
    assert_difference "Contract.count", 2 do
      contract = Contract.create(number: "A10", active: true, start_date: "2020-02-01", end_date: "2020-03-01", fixed_fee: 1.9, days_included: 14, additional_fee: 0.1)
    end

    contract2.reload
    assert_equal contract2.active, true
    assert_equal contract2.end_date, contract.start_date - 1
  end

  test "should split old contract" do
    contract = nil
    contract1 = contracts(:one)
    assert_difference "Contract.count", 2 do
      contract = Contract.create(number: "A10", active: true, start_date: "2019-02-01", end_date: "2019-03-01", fixed_fee: 1.9, days_included: 14, additional_fee: 0.1)
    end

    contract1.reload
    assert_equal contract1.active, true
    assert_equal contract1.end_date, contract.start_date - 1
  end

  test "should not update not overlapping contracts" do
    contract1 = contracts(:one)
    start_date = contract1.start_date
    end_date = contract1.end_date
    Contract.create(number: "A10", active: true, start_date: "2020-02-01", end_date: "2020-03-01", fixed_fee: 1.9, days_included: 14, additional_fee: 0.1)

    contract1.reload
    assert_equal start_date, contract1.start_date
    assert_equal end_date, contract1.end_date
  end
end
