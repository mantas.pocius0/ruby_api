require 'test_helper'

class InvoicesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @invoice = Invoice.new(
      contract_number: "A10",
      issue_date: "2019-01-01",
      due_date: "2019-01-15",
      paid_date: "2019-01-20",
      purchase_date: "2019-01-05",
      invoice_amount: 1000
    )
  end

  test "should create invoice" do
    assert_difference('Invoice.count') do
      post invoices_url, 
        params: { 
          invoice: { 
            invoice_amount: @invoice.invoice_amount, 
            contract_number: @invoice.contract_number, 
            issue_date: @invoice.issue_date, 
            due_date: @invoice.due_date, 
            paid_date: @invoice.paid_date, 
            purchase_date: @invoice.purchase_date 
          } 
        }, as: :json
    end
    parsed_response = JSON.parse(@response.body)

    assert_response 201
    assert_equal 22, parsed_response["fee"].to_f
  end
end
