require 'test_helper'

class ContractsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @contract = contracts(:one)
  end

  test "should create contract" do
    assert_difference('Contract.count') do
      post contracts_url, params: { contract: { active: @contract.active, additional_fee: @contract.additional_fee, days_included: @contract.days_included, end_date: @contract.end_date, fixed_fee: @contract.fixed_fee, number: @contract.number, start_date: @contract.start_date } }, as: :json
    end

    assert_response 201
    assert_not @contract.reload.active
  end
end
