class CreateContracts < ActiveRecord::Migration[6.0]
  def change
    create_table :contracts do |t|
      t.string :number, null: false
      t.boolean :active, null: false
      t.date :start_date, null: false
      t.date :end_date
      t.decimal :fixed_fee, null: false, precision: 8, scale: 4
      t.integer :days_included, null: false
      t.decimal :additional_fee, null: false, precision: 8, scale: 4

      t.timestamps
    end

    add_index :contracts, :number
  end
end
