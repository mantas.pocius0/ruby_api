class CreateInvoices < ActiveRecord::Migration[6.0]
  def change
    create_table :invoices do |t|
      t.references :contract, null: false, foreign_key: true
      t.date :issue_date, null: false
      t.date :due_date, null: false
      t.date :paid_date
      t.date :purchase_date, null: false
      t.decimal :invoice_amount, null: false, precision: 10, scale: 2
      t.decimal :fee, null: false, precision: 8, scale: 2

      t.timestamps
    end
  end
end
